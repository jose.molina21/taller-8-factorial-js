//Pedir número al usuario
let n = Number(prompt("Ingresa un número"));

//Calcular factorial
function factorial(numero) {
    if (numero <= 1) {
        return 1;
    }
    return numero * factorial(numero - 1);
}
// Mostrar factorial
alert("El factorial es " + factorial(n));